<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Minton - Responsive Admin Dashboard Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('ui')}}/{{asset('ui')}}/assets/images/favicon.ico">

        <!-- App css -->
        <link href="{{asset('ui')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{asset('ui')}}/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{asset('ui')}}/assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            @include('layout/partials/header')
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            @include('layout/partials/side-bar')
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            @yield('content')

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        @include('layout/partials/footer')
        <!-- Right Sidebar -->
       @include('layout/partials/right-side-bar')
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('ui')}}/assets/js/vendor.min.js"></script>

        <script src="{{asset('ui')}}/assets/libs/jquery-knob/jquery.knob.min.js"></script>
        <script src="{{asset('ui')}}/assets/libs/peity/jquery.peity.min.js"></script>

        <!-- Sparkline charts -->
        <script src="{{asset('ui')}}/assets/libs/jquery-sparkline/jquery.sparkline.min.js"></script>

        <!-- init js -->
        <script src="{{asset('ui')}}/assets/js/pages/dashboard-1.init.js"></script>

        <!-- App js -->
        <script src="{{asset('ui')}}/assets/js/app.min.js"></script>

    </body>
</html>
